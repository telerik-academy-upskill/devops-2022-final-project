# DevOps 2022 Final Project

# Summary
This repository was created for educational purposes! 
It simulates close to а real development lifecycle of .NET6.0 API using Trunk-based approach.

#

- .NET6.0 BookStoreApi performing basic CRUD operations
- Tests:
    - Unit tests - dummy test only created for the purpose of adding it to the pipline
    - Integratin tests - dummy test only created for the purpose of adding it to the pipline
    - System tests - here few tests running against deployed API instance
        - System tests read their configuration (API BaseURL) from Vault cluster in https://cloud.hashicorp.com/
- Builds basic docker image for BookStoreApi
- Implements pipline with several stages in the following order:
    - build-validation stage: It aims to validate .NET6.0 sourc code for any errors and if any to stop the pipline run
    - test stage: 
        - Runs unit & integration tests projects
        - Runs Static Application Security Testing (SAST)
    - versioning stage: creates a version tag related to the commit
    - build-api-artifacts: creates an artifact needed for next stage where Docker immage is build
    - build-docker-image: builds and pushes docker immage to remote ECR repository
    - system-test: this stage aims to validate the deployment as running tests against running in AWS App Runner instance of BookStoreApi
    


