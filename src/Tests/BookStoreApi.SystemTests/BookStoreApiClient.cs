﻿using System.Net.Http;
using TestUtilities;

namespace BookStoreApi.SystemTests
{
    public class BookStoreApiClient : IBookStoreApiClient
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public virtual HttpResponseMessageWrapper Get(string baseUrl)
        {
            var test = _httpClient.GetAsync(baseUrl + "api/books").Result;
            var response = HttpResponseMessageWrapper.ToHttpResponseMessageWrapper(test);

            return response;
        }

       public HttpResponseMessageWrapper Get(string baseUrl, string id)
        {
            var getAsyncResult = _httpClient.GetAsync(baseUrl + $"api/books/{id}").Result;
            var response = HttpResponseMessageWrapper.ToHttpResponseMessageWrapper(getAsyncResult);

            return response;
        }

        public HttpResponseMessageWrapper Post(string baseUrl, StringContent requestBody)
        {
            var postAsyncResult = _httpClient
                .PostAsync(baseUrl + "api/Books", requestBody)
                .Result;
            var response = HttpResponseMessageWrapper.ToHttpResponseMessageWrapper(postAsyncResult);

            return response;
        }
    }
}
