﻿using System.Net.Http;
using TestUtilities;

namespace BookStoreApi.SystemTests
{
    public interface IBookStoreApiClient
    {
        public HttpResponseMessageWrapper Get(string baseUrl);

        public HttpResponseMessageWrapper Get(string baseUrl, string id);

        public HttpResponseMessageWrapper Post(string baseUrl, StringContent requestBody);
    }
}
