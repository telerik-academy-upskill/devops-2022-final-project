﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using TestUtilities;

namespace BookStoreApi.SystemTests.TestsInfrastructure
{
    public class IoCFactory
    {
        public static ServiceProvider Create()
        {
            var mongoConnectionString =
                "mongodb+srv://playground:tINpmI5FoGktcapd@rt-mongodb-playground.caxjlpd.mongodb.net/?retryWrites=true&w=majority";

            return new ServiceCollection()
                .AddSingleton<VaultClient>()
                .AddSingleton<BookStoreApiClient>()
                .AddSingleton<MongoClient>(new MongoClient(mongoConnectionString))
                .BuildServiceProvider();
        }
    }
}
