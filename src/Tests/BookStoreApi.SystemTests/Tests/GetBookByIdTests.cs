﻿using FluentAssertions;
using NUnit.Framework;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using TestUtilities;

namespace BookStoreApi.SystemTests.Tests
{
    public class GetBookByIdTests : BaseTest
    {
        [Test]
        public void GetBook_FromDb_ById()
        {
            //Arrange
            var baseUrl = VaultClient.GetConfiguration("system-tests", Token).BaseUrl;
            StringContent bookToPost = new(
                 JsonSerializer.Serialize(new
                 {
                     Name = "Test." + Faker.Name,
                     Price = Faker.Random.Decimal(1, 100),
                     Category = Faker.Name.ToString(),
                     Author = "Test." + Faker.Person
                 }),
                 Encoding.UTF8,
                 "application/json");
            var createdBookStringResponse = BookStoreApiClient.Post(baseUrl, bookToPost);
            var createdBookResponse = JsonSerializer.Deserialize<Book>(createdBookStringResponse.Body);

            //Act
            var retrievedBookStringResponse = BookStoreApiClient.Get(baseUrl, createdBookResponse!.Id);

            //Assert
            var retrievedBookResponse = JsonSerializer.Deserialize<Book>(retrievedBookStringResponse.Body);
            retrievedBookResponse.Should().BeEquivalentTo(createdBookResponse);
        }
    }
}