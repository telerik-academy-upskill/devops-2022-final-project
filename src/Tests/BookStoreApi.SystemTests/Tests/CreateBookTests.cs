﻿using FluentAssertions;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using TestUtilities;

namespace BookStoreApi.SystemTests.Tests
{
    public class CreateBookTests : BaseTest
    {
        [Test]
        public void CreateBook_InDb()
        {
            //Arrange
            var baseUrl = VaultClient.GetConfiguration("system-tests", Token).BaseUrl;
            StringContent bookToPost = new(
                JsonSerializer.Serialize(new
                {
                    Name = "Test." + Faker.Name,
                    Price = Faker.Random.Decimal(1, 100),
                    Category = Faker.Name.ToString(),
                    Author = "Test." + Faker.Person
                }),
                Encoding.UTF8,
                "application/json");

            //Act
            var createdBookResponse = BookStoreApiClient.Post(baseUrl, bookToPost);
            var createdBookResponseBody = JsonSerializer.Deserialize<Book>(createdBookResponse.Body);

            //Assert
            createdBookResponse.Response.StatusCode.Should().Be(HttpStatusCode.Created);

            var postedBook = JsonSerializer.Deserialize<Book>(bookToPost.ReadAsStringAsync().Result);
            createdBookResponseBody.Should().BeEquivalentTo(postedBook, o => o.Excluding(p => p.Id));
        }
    }
}