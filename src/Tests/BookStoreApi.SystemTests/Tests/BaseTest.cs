﻿using Bogus;
using BookStoreApi.SystemTests.TestsInfrastructure;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using NUnit.Framework;
using TestUtilities;

namespace BookStoreApi.SystemTests.Tests
{
    public class BaseTest
    {
        private ServiceProvider _serviceProvider;

        protected Faker Faker => new Faker();

        protected BookStoreApiClient BookStoreApiClient => _serviceProvider.GetService<BookStoreApiClient>();

        protected VaultClient VaultClient => _serviceProvider.GetService<VaultClient>();

        protected string Token => VaultClient.GetToken();

        private MongoClient MongoClient => _serviceProvider.GetService<MongoClient>();

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _serviceProvider = IoCFactory.Create();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            var mongoDatabase = MongoClient.GetDatabase("BookStore");
            var mongoCollection = mongoDatabase.GetCollection<Book>("Books");
            mongoCollection.DeleteMany(r => r.Author.StartsWith("Test."));

            _serviceProvider.Dispose();
        }
    }
}
