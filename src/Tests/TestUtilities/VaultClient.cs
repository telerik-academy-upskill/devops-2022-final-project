﻿using Flurl.Http;
using NUnit.Framework;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace TestUtilities
{
    public class VaultClient : IVaultClient
    {
        private readonly string _baseUrl = TestContext.Parameters["VaultUrl"];

        public BookStoreApiSection GetConfiguration(string configurationPath, string token)
        {
            var targetUrl = _baseUrl + $"/secret/data/{configurationPath}";
            var request = targetUrl.AllowAnyHttpStatus();
            request.Headers.Add("X-Vault-Token", token);
            request.Headers.Add("X-Vault-Namespace", "admin");
            var getAsyncResult = request.GetAsync().Result.ResponseMessage;

            var getConfigurationResponse = HttpResponseMessageWrapper.ToHttpResponseMessageWrapper(getAsyncResult);

            var configuration = JsonSerializer.Deserialize<VaultSecretResult>(getConfigurationResponse.Body);

            return configuration.Data.DataLevelTwo.BookStoreApiSection;
        }

        public string GetToken()
        {
            StringContent body = new(
                JsonSerializer.Serialize(new
                {
                    role_id = TestContext.Parameters["VaultRoleId"],
                    secret_id = TestContext.Parameters["VaultSecredId"]
                }),
                Encoding.UTF8,
                "application/json");

            var targetUrl = _baseUrl + "/auth/approle/login";
            var request = targetUrl.AllowAnyHttpStatus();
            request.Headers.Add("X-Vault-Namespace", "admin");
            var postAsyncResult = request.PostAsync(body).Result.ResponseMessage;

            var response = HttpResponseMessageWrapper.ToHttpResponseMessageWrapper(postAsyncResult);
            var token = JsonSerializer.Deserialize<VaultLoginResult>(response.Body)
                .Auth.ClientToken;

            return token;
        }
    }

    public class BookStoreApiSection
    {
        public string BaseUrl { get; set; }
    }

    public class VaultSecretResult
    {
        [JsonPropertyName("data")]
        public Data Data { get; set; }
    }

    public class Data
    {
        [JsonPropertyName("data")]
        public DataLevelTwo DataLevelTwo { get; set; }
    }

    public class DataLevelTwo
    {
        public BookStoreApiSection BookStoreApiSection { get; set; }
    }

    public class VaultLoginResult
    {
        [JsonPropertyName("auth")]
        public Auth Auth { get; set; }
    }

    public class Auth
    {
        [JsonPropertyName("client_token")]
        public string ClientToken { get; set; }
    }
}
