﻿using System.Text.Json.Serialization;

namespace TestUtilities;

public class Book
{
    //[JsonPropertyName("id")]
    public string Id { get; set; }

    [JsonPropertyName("Name")]
    public string BookName { get; set; }
    
    //[JsonPropertyName("price")]
    public decimal Price { get; set; }

    //[JsonPropertyName("category")]
    public string Category { get; set; }

    //[JsonPropertyName("author")]
    public string Author { get; set; }
}