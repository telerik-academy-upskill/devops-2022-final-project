﻿namespace TestUtilities
{
    public interface IVaultClient
    {
        public BookStoreApiSection GetConfiguration(string configurationPath, string token);
    }
}
