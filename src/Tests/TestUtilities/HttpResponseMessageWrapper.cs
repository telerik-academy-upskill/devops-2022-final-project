﻿using System.Net.Http;

namespace TestUtilities
{
    public class HttpResponseMessageWrapper
    {
        public HttpResponseMessage Response { get; set; }

        public string Body { get; set; }

        public HttpResponseMessageWrapper(
            HttpResponseMessage response)
            : this(response, response.Content.ReadAsStringAsync().Result)
        {
        }

        public HttpResponseMessageWrapper(
            HttpResponseMessage response,
            string body)
        {
            Response = response;
            Body = body;
        }

        public static HttpResponseMessageWrapper ToHttpResponseMessageWrapper(HttpResponseMessage httpResponseMessage)
        {
            return new HttpResponseMessageWrapper(httpResponseMessage);
        }
    }
}
