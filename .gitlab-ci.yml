stages:
  - build-validation
  - test
  - versioning
  - build-api-artifacts
  - build-docker-image
  - system-test

build-validation:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: build-validation
  when: always
  before_script:
    - 'dotnet restore ./src --packages ./.nuget/'
  script:
    - 'dotnet build ./src --no-restore'
  except:
    - tags
sast:
  stage: test
  needs: ["build-validation"]
include:
  - template: Security/SAST.gitlab-ci.yml

unit-tests:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: test
  needs: ["build-validation"]
  when: always
  before_script:
    - dotnet restore ./src --packages ./.nuget/
  script:
    - dotnet test ./src/Tests/$TEST_PROJECT_NAME/$TEST_PROJECT_NAME.csproj -c Release --no-restore --logger "console;verbosity=detailed"
  variables: 
    TEST_PROJECT_NAME: BookStoreApi.UnitTests
  except:
    - tags

integration-tests:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: test
  needs: ["build-validation"]
  when: always
  before_script:
    - dotnet restore ./src --packages ./.nuget/
  script:
    - dotnet test ./src/Tests/$TEST_PROJECT_NAME/$TEST_PROJECT_NAME.csproj -c Release --no-restore --logger "console;verbosity=detailed"
  variables: 
    TEST_PROJECT_NAME: BookStoreApi.IntegrationTests
  except:
    - tags

system-tests:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: system-test
  when: manual
  before_script:
    - dotnet restore ./src --packages ./.nuget/
  script:
    - >
      dotnet test ./src/Tests/$TEST_PROJECT_NAME/$TEST_PROJECT_NAME.csproj 
      -c Release --no-restore --logger "console;verbosity=detailed"
      --settings $RUNSETTINGS_FILE_PATH -- TestRunParameters.Parameter\(name=\"VaultRoleId\",\ value=\"$VAULT_ROLE_ID\"\) TestRunParameters.Parameter\(name=\"VaultSecredId\",\ value=\"$VAULT_SECRED_ID\"\) TestRunParameters.Parameter\(name=\"VaultUrl\",\ value=\"$VAULT_URL\"\)
  variables: 
    TEST_PROJECT_NAME: BookStoreApi.SystemTests
    RUNSETTINGS_FILE_PATH: src/test.runsettings
  only:
    - tags

versioning:
  image: 
    name: gittools/gitversion:5.8.1
    entrypoint: [""]
  stage: versioning
  script:
     - TAG=$(/tools/dotnet-gitversion -updateassemblyinfo -showvariable SemVer)
     - export TAG=${TAG_PREFIX}${TAG}
     - |
       export URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags
       wget --method POST \
         --timeout=0 \
         --header "PRIVATE-TOKEN: ${GITLAB_AUTH_TOKEN:?}" \
         --body-data "message=${CI_COMMIT_MESSAGE}&tag_name=${TAG}&ref=${CI_BUILD_REF}" \
         ${URL}
  variables: 
    GIT_DEPTH: 0
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: manual
  dependencies: []

build-api-artifacts:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: build-api-artifacts
  when: always
  script:
    - dotnet restore ./src --packages ./.nuget/
    - dotnet build ./src --no-restore 
  artifacts:
    expire_in: 1 hour
    paths:
      - ./src/BookStoreApi/
  only:
    - tags

build-docker-image:
  image: docker:stable-dind
  stage: build-docker-image
  dependencies: ["build-api-artifacts"]
  when: always
  script:
    - apk update
    - apk add --no-cache git openssh-client
    - apk add py-pip
    - pip install --upgrade awscli
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set default.region eu-west-1 
    - aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin $ECR_REPOSITORY
    - echo Build and publish docker Image
    - BRANCH_NAME=$(git ls-remote --heads origin | grep $CI_COMMIT_SHA | sed "s/[^/]*\/[^/]*\///")
    - >
      if [[ "$BRANCH_NAME" != "$CI_DEFAULT_BRANCH" ]]; then
          docker build -t $DOCKER_IMAGE_TAG $ARTIFACTS_PATH
          docker push $DOCKER_IMAGE_TAG || { echo -e '\e[1;31mDocker push failed\e[0m' ; exit 1; }
      else
          docker build -t $DOCKER_IMAGE_LATEST_TAG -t $DOCKER_IMAGE_TAG $ARTIFACTS_PATH
          docker push $DOCKER_IMAGE_LATEST_TAG || { echo -e '\e[1;31mDocker push failed\e[0m' ; exit 1; }
          docker push $DOCKER_IMAGE_TAG || { echo -e '\e[1;31mDocker push failed\e[0m' ; exit 1; }
      fi
  variables:
    DOCKER_IMAGE_TAG: $ECR_REPOSITORY:$CI_COMMIT_TAG
    DOCKER_IMAGE_LATEST_TAG: $ECR_REPOSITORY:latest 
    ARTIFACTS_PATH: ./src/BookStoreApi/
  services:
    - docker:dind
  only:
    - tags


# Templates

.install-terraform:
  - wget https://releases.hashicorp.com/terraform/1.3.7/terraform_1.3.7_linux_amd64.zip -P /tmp
  - unzip /tmp/terraform_1.3.7_linux_amd64.zip -d /usr/local/bin/
